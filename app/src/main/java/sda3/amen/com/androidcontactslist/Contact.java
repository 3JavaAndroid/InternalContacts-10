package sda3.amen.com.androidcontactslist;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 6/7/17.
 */

public class Contact {
    private int id;
    private String name;
    private List<String> numbers;

    public Contact(int id, String name) {
        this.id = id;
        this.name = name;
        this.numbers = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<String> numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numbers=" + numbers +
                '}';
    }
}
