package sda3.amen.com.androidcontactslist;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.listView)
    protected ListView listView;

    @OnItemClick(R.id.listView)
    protected void listItemClick(int position) {
        Contact clickedContact = contactsAdapter.getItem(position);
        String numberToPass = clickedContact.getNumbers().get(0);

        // start activity, przekazujemy id kontaktu
        Intent i = new Intent(this, MessagesActivity.class);
        i.putExtra("number", numberToPass);
        startActivity(i);
    }

    private ArrayAdapter<Contact> contactsAdapter;

    private boolean permissionsChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        contactsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                new ArrayList<Contact>());

        listView.setAdapter(contactsAdapter);

        isPermissionGranted();
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshContacts();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            permissionsChecked = true;
            refreshContacts();
        }
    }

    private void refreshContacts() {
        if (!permissionsChecked) {
            return;
        }
        contactsAdapter.clear();

        Uri contactsURI = ContactsContract.Contacts.CONTENT_URI;
        Uri phoneNumbersURI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        ContentResolver resolver = getContentResolver();
        Cursor contactsCursor = resolver.query(contactsURI, null, null, null, null);

        // numery kolumn które interesują nas w kontaktach
        int contactNameColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int contactHasNumberColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int contactIdColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts._ID);

        String phoneContactIdColumn = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;

        for (int i = 0; i < contactsCursor.getCount(); i++) {
            contactsCursor.moveToNext();

            int contactId = contactsCursor.getInt(contactIdColumnId);
            String contactName = contactsCursor.getString(contactNameColumnId);
            int contactPhoneNumbers = contactsCursor.getInt(contactHasNumberColumnId);

            Contact contact = new Contact(contactId, contactName);

            //************************************************
            Cursor phoneNumbersCursor = resolver.query(phoneNumbersURI,
                    null,                                       // kolumny
                    phoneContactIdColumn + "=?",                // klauzula where - po czym wyszukujemy
                    new String[]{String.valueOf(contactId)},    // argumenty których szukamy
                    null);                                      // kolejność

            // numery kolumn które interesują nas w numerach telefonów wybranych kontaktów
            int phoneNumberColumnId = phoneNumbersCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            // iterujemy numery telefonów
            for (int j = 0; j < phoneNumbersCursor.getCount(); j++) {
                phoneNumbersCursor.moveToNext();

                String phoneNumber = phoneNumbersCursor.getString(phoneNumberColumnId);

                // dodanie numeru do modelu kontaktu
                contact.getNumbers().add(phoneNumber);
            }
            phoneNumbersCursor.close();
            //*************************************************

            contactsAdapter.add(contact);
        }
        contactsCursor.close();
    }

    private void isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                permissionsChecked = true;
                Log.v(getClass().getName(), "Permission is granted");
            } else {
                Log.v(getClass().getName(), "Permission is revoked");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.READ_SMS},
                        1);
            }
        } else {
            Log.v(getClass().getName(), "Permission is granted");
        }
    }
}
